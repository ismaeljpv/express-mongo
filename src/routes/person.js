const { Router } = require('express');
const router = Router();

//QueryString => query property on the request object
// ?paramName=paramValue
router.get('/person', (req,res) => {
    if(req.query.name){
        res.send(`You have a request from :${req.query.name}`);
    }else{
        res.send('request recieved');
    }
});

//Params property on the request object
router.get('/person/:name', (req, res) => {
    res.send(`You have a request from :${req.params.name}`);
});

router.get('/error', (req, res) => {
    throw new Error('This is a forced error.');
});

module.exports = router;