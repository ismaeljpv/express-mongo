let CustomerModel = require('../models/customer-model');
const { Router } = require('express');
const router = Router();

//Create a new Customer
router.post('/customer', (req, res)=>{

        if(!req.body.email || !req.body.name){
            throw new Error('There is data missing in the body request');
        }

        let model = new CustomerModel(req.body);
        model.save(function (err, mod) {
            if (err) { 
                throw new Error(err.toString());
            }
            return res.status(201).json(mod);    
        });
    
});

// Get Customer
router.get('/customer', (req, res) =>{
 
    if(!req.query.email){
        throw new Error('There is data missing in request');
    }
    let qEmail = req.query.email;
    CustomerModel.findOne({email: qEmail}, (err, data) => {
        if (err) { 
            throw new Error(err.toString());
        }
        return res.status(201).json(data);    
    });
});

//Update customer
router.put('/customer', (req, res)=>{

    if(!req.query.email){
        throw new Error('There is data missing in request');
    }
    let qEmail = req.query.email;
    CustomerModel.findOneAndUpdate(
        { email: qEmail },
          req.body,
        { new: true }
        )
        .then( doc =>{
            res.json(doc);
        })
        .catch(err => {
            throw new Error(err.toString()); 
        });

});

//Delete customer
router.delete('/customer', (req, res) => {

    if(!req.query.email){
        throw new Error('There is data missing in request');
    }
    let qEmail = req.query.email;
    CustomerModel.findOneAndRemove(
        { email: qEmail }
        )
        .then( doc =>{
            res.json(doc);
        })
        .catch(err => {
            throw new Error(err.toString()); 
        });
});

module.exports = router;