const express = require('express');
const app = express();
//const path = require('path');

//DB Connection
require('./db/connection');

//Routes
let personRoute = require('./routes/person');
let customerRoute = require('./routes/customer');

//Settings
app.set('port', process.env.PORT || 3000);

// Middlewares
app.use(express.json());

//Routes
app.use(personRoute);
app.use(customerRoute);

//Public
app.use(express.static('public'));

//Starting the server
app.listen(app.get('port'), () =>{
    console.log(`Server on Port ${app.get('port')}`);
});