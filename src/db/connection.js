let mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/mycustomers', 
{   useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
});

module.exports = mongoose;